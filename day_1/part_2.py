#!/usr/bin/python3

f = open('input.txt', 'r')
expenses = f.readlines()
expenses = [int(expense) for expense in expenses]

for i in range (len(expenses)):
    for j in range(i+1, len(expenses)):
        for k in range(j+1, len(expenses)):
            if expenses[i] + expenses[j] + expenses[k] == 2020:
                print(str(expenses[i]) + " x " + str(expenses[j]) + " x " + str(expenses[k]) + " = " + str(expenses[i]*expenses[j]*expenses[k]))
