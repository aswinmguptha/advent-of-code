#!/usr/bin/python3

f = open('input.txt', 'r')
expenses = f.readlines()
expenses = [int(expense) for expense in expenses]

for i in range (len(expenses)):
    for j in range(i+1, len(expenses)):
        if expenses[i] + expenses[j] == 2020:
            print(str(expenses[i]) + " x " + str(expenses[j]) + " = " + str(expenses[i]*expenses[j]))
