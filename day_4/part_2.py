#!/usr/bin/python3

import string
f = open('input.txt', 'r')
inputs = f.readlines()
passports = []
dictionary = {}
count = 0

def is_valid(passport):
    count = 0
    if int(passport['byr']) in range(1920, 2003):
        count +=1
    if int(passport['iyr']) in range(2010, 2021):
        count +=1
    if int(passport['eyr']) in range(2020, 2031):
        count+=1
    if passport['hgt'][-2:] == 'cm':
        if int(passport['hgt'][:-2]) in range(150, 194):
            count+=1
    elif passport['hgt'][-2:] == 'in':
        if int(passport['hgt'][:2]) in range(59, 77):
            count+=1
    if all(c in string.hexdigits for c in passport['hcl'][1:]) and len(passport['hcl']) == 7:
        count+=1
    if passport['ecl'] in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        count+=1
    if len(passport['pid']) == 9:
        count+=1
    if count == 7:
        return True
    else:
        return False

for i in range(len(inputs)):
    if inputs[i] != '\n':
        entries = inputs[i].split()
        for j in range(len(entries)):
            entry = entries[j].split(':')
            dictionary[entry[0]] = entry[1]
    else:
        passports.append(dictionary)
        dictionary = {}
passports.append(dictionary)

for i in range(len(passports)):
    if len(passports[i]) >= 7:
        if len(passports[i]) == 8:
            if is_valid(passports[i]):
                count+=1
        else:
            if 'cid' not in passports[i]:
                if is_valid(passports[i]):
                    count+=1
print(count)
