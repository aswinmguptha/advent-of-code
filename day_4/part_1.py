#!/usr/bin/python3

f = open('input.txt', 'r')
inputs = f.readlines()
passports = []
dictionary = {}
count = 0

for i in range(len(inputs)):
    if inputs[i] != '\n':
        entries = inputs[i].split()
        for j in range(len(entries)):
            entry = entries[j].split(':')
            dictionary[entry[0]] = entry[1]
    else:
        passports.append(dictionary)
        dictionary = {}
passports.append(dictionary)

for i in range(len(passports)):
    if len(passports[i]) >= 7:
        if len(passports[i]) == 8:
            count+=1
        else:
            if 'cid' not in passports[i]:
                count+=1
print(count)
