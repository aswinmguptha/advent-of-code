#!/bin/env python3

f = open('input.txt', 'r')
inputs = f.readlines()
answers = []
answer = []
count = 0

for i in range(len(inputs)):
        if inputs[i] != '\n':
            answer.append(inputs[i].split()[-1])
        else:
            answers.append(answer)
            answer = []
answers.append(answer)
for i in range(len(answers)):
    count += len(list(set(''.join(answers[i]))))
print(count)
