#!/bin/env python3

f = open('input.txt', 'r')
inputs = f.readlines()
answers = []
answer = []
count = 0

for i in range(len(inputs)):
        if inputs[i] != '\n':
            answer.append(inputs[i].split()[-1])
        else:
            answers.append(answer)
            answer = []
answers.append(answer)
for i in range(len(answers)):
    for j in range(len(answers[i])):
        answers[i][j] = set(answers[i][j])
    count += len(set.intersection(*answers[i]))
print(count)
