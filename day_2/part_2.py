#!/usr/bin/python3

f = open('input.txt', 'r')
passwords = f.readlines()
count = 0
for i in range(len(passwords)):
    valid_position = int(passwords[i].split(" ")[0].split("-")[0])
    invalid_position = int(passwords[i].split(" ")[0].split("-")[1])
    password = passwords[i].split(" ")[2].split()[0]
    policy = passwords[i].split(" ")[1][:1]
    if password[valid_position-1] == policy or password[invalid_position-1] == policy:
        if password[valid_position-1] == policy and password[invalid_position-1] == policy:
            continue
        count += 1
print(count)
