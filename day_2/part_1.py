#!/usr/bin/python3

f = open('input.txt', 'r')
passwords = f.readlines()
count = 0
for i in range(len(passwords)):
    minimum = int(passwords[i].split(" ")[0].split("-")[0])
    maximum = int(passwords[i].split(" ")[0].split("-")[1])
    password = passwords[i].split(" ")[2].split()[0]
    policy = passwords[i].split(" ")[1][:1]
    occurrence = password.count(policy)
    if occurrence >= minimum and occurrence <= maximum:
        count += 1
print(count)
