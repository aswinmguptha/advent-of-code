#!/usr/bin/python3

def count_trees(right, down):
    f = open('input.txt', 'r')
    geology = f.readlines()
    
    position = 0
    count = 0
    columns = len(geology[0])-1
    
    for row in range(0,len(geology),down):
        if geology[row][position%columns] == '#':
            count += 1
        position += right
    return count

print(count_trees(1,1) * count_trees(3,1) * count_trees(5,1) * count_trees(7,1) * count_trees(1,2))
