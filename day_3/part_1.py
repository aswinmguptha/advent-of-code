#!/usr/bin/python3

f = open('input.txt', 'r')
geology = f.readlines()

position = 0
count = 0

for i in range(1, len(geology)):
    geology[i] = geology[i][:-1]
    position += 3
    while position > len(geology[i]):
        geology[i] += geology[i]
    if geology[i][position] == '#':
        count += 1
print(count)
