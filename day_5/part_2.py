#!/bin/env python3

import math
f = open('input.txt', 'r')
seats = f.readlines()
seat_id = []

def missing(seat_id):
    return [ x for x in range(seat_id[0], seat_id[-1]+1) if x not in seat_id ]

for i in range(len(seats)):
    row_lower = 0
    row_upper = 127
    for j in range(0, 7):
        if seats[i][j] == 'F':
            row_upper = math.floor(row_lower + ((row_upper - row_lower)/2))
        else:
            row_lower = math.ceil((row_lower + row_upper)/2)

    column_lower = 0
    column_upper = 7
    for k in range(7, 10):
        if seats[i][k] == 'L':
            column_upper = math.floor(column_lower + ((column_upper - column_lower)/2))
        else:
            column_lower = math.ceil((column_lower + column_upper)/2)
    seat_id.append((row_upper*8) + column_upper)

seat_id.sort()
print(missing(seat_id)[0])
